<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'aktie' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'GQ,;^qAA+hWix5mbJ]Q{&6Z/_VJriopvw#qtB7tEzVYEW)wWnU,tayPVCykIoZ3P' );
define( 'SECURE_AUTH_KEY',  'q_Cd_W!M(}is9,BN@rL26:4Q/zD1Y]ejUvVWGNE`[>x3n5:X~*a*bj|I?%Wv_|R2' );
define( 'LOGGED_IN_KEY',    'g:q2r)=s4VJua9FRZT4kUk%$5WMV0GLdmS)e:@ENM=lSFk|8^7C1m,F6*hxe2nBJ' );
define( 'NONCE_KEY',        'R$w]Y=jstC/]_ybyw}x6:?~K$.0oGNaQjf/iK,&mpLj~J#Cl]o67vT(sK6k+9&:&' );
define( 'AUTH_SALT',        'q).oZesfn vvO$W6ijn}JIyxfpg@/w{aISdm-i>MlK5#uzw!;(SJpIqR!#R4rMTZ' );
define( 'SECURE_AUTH_SALT', '>!Ui1DWa-ol6m%@(q|}TzqC0]$Yp*1.sS8]YjN.Iq4=|V xIIUQNGSrUb+|SlpA,' );
define( 'LOGGED_IN_SALT',   'dHv2=ItW(aB5Qc`=kLIP~/;r2mY`lM6)t:jAxbHC#u/(SJxYTHrLs[8}cWi[rtRn' );
define( 'NONCE_SALT',       'nlcC%HuWV*x~/kD73mH-w0`{7WLaL3{gkHE!Y<06=Il},9MbfDD^~d5N>Be+J2vN' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
